# Revision history for nix-firefox-addons

## Unreleased

* Bump `relude` version, from 0.4.0 to 1.0.0.

## 0.1.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.
