{ pkgs ? import <nixpkgs> {}
, ghc ? "ghc8104"
}:
let
  haskellPackages = pkgs.haskell.packages.${ghc}.override {
    overrides = self: super: rec {
      # TODO: Remove this override when haskellPackages.relude >= 1.0.0.
      relude = super.relude_1_0_0_1;
    };
  };
in haskellPackages.developPackage {
  root = ./.;
  modifier = drv:
    pkgs.haskell.lib.overrideCabal drv (attrs: {
      buildTools = (attrs.buildTools or [ ]) ++ [
        haskellPackages.cabal-install
        haskellPackages.haskell-language-server
        pkgs.nixfmt
      ];
    });
}
