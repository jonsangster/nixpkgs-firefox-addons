{
  description = "A tool to generate a Nix package set of Firefox add-ons.";

  inputs = {
    flake-utils.url = github:numtide/flake-utils;
  };

  outputs = { self, nixpkgs, flake-utils }:
    let
      packageName = "firefox-addons-generator";
      ghc = "ghc8104";

      overlay = final: prev: {
        ${packageName} =
          let
            haskellPackages = final.haskell.packages.${ghc}.override {
              overrides = hself: hsuper: {
                # TODO: Remove this override when haskellPackages.relude >= 1.0.0.
                relude = hsuper.relude_1_0_0_1;
              };
            };
          in haskellPackages.callCabal2nix packageName self {};
      };
    in
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; overlays = [ overlay ]; };
      in {
        defaultPackage          = pkgs.${packageName};
        packages.${packageName} = pkgs.${packageName};
      }
    ) // { inherit overlay; };
}
